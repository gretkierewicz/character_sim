# Character Simulator
Character simulator will consist of complex character generator including personality, historical records or traits.
It will also allow for simulation of character's interaction with environment/events.

---
### Subject

Due to my interest to human psychology I would like to create character generator using advanced concepts like:
- MBTI (Meyers-Briggs Type Indicator) types / Jung's Cognitive Functions and related traits of each
- at least simple emotions sim to allow better responses to events
- using Cognitive Triangle (CBT Triangle) as at least one of "act on event" strategies

The main purpose of it is to make character generator that creates "psycho sim"
with steady and consistent responses to events.
Output should provide character's simulated thoughts or feelings as well. 

Additional idea is to start with childhood where mind is ductile.
With major life events making great impact on reshaping personality and creating adult
with more fixed version of it.

It shouldn't be considered as an attempt of recreating human true psyche mechanisms.
Some implementations can be very far away concept of what we know about our thoughts or feelings.
What I want to achieve is mimicking it this way or another, what will fit best for particular purpose.

---
### Gain

Expected gain from that project is developing skills in program development, designing architecture, 
training good practices approach, managing project, writing tests. 
Nonetheless, gaining more knowledge about human psychology is an attractive asset as well.

---
